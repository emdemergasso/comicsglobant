package com.globant.academy.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import com.globant.academy.utils.Constant;

public class ListLoans {

	private ArrayList<Loans> loanList = new ArrayList<Loans>();

	public boolean addLoan(String dni, String comicName, int cost, boolean returned) {

		try {

			loanList.add(new Loans(dni, comicName, cost, returned));
			return true;

		} catch (Exception e) {

			Constant.print("Something went wrong, try again.");
			e.printStackTrace();
			return false;
		}
	}

	public void getAllLoans() {

		Constant.print("Dni | Comic Name | Cost | Returned");

		for (Loans loans : loanList) {

			System.out.println(loans.getDni() + " | " + loans.getComicName() + " | " + loans.getCost() + " | "
					+ loans.isReturned());
		}
	}

	public void deleteLoanByDni() {

		Constant.print("Delete loan from user by dni");
		Constant.print("Input dni");
		Scanner myScanner = new Scanner(System.in);
		String dni = myScanner.nextLine();

		try {

			Loans loansDni = new Loans();
			loansDni.setDni(dni);

			if (loanList.contains(loansDni)) {

				System.out.println("Yes");
				loanList.remove(loansDni);
			} else {
				System.out.println("No");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
