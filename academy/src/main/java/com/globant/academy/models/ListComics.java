package com.globant.academy.models;

import java.util.ArrayList;
import java.util.Scanner;

import com.globant.academy.utils.Constant;

public class ListComics {

	private ArrayList<Comics> comicList = new ArrayList<Comics>();

	public boolean addComic(String name, String genre, String editorial,
			int quantity) {

		try {

			comicList.add(new Comics(name, genre, editorial, quantity));
			return true;

		} catch (Exception e) {

			Constant.print("Something went wrong.");
			e.printStackTrace();
			return false;
		}
	}

	public void getAllComics() {
		
		Constant.print("Name - Genre - Editorial - Quantity");
		
		for (Comics comics: comicList) {
			
			System.out.println("\n"+comics);
		}
	}
	
	public void deleteComicByName() {

		Constant.print("Delete comic by name");
		Constant.print("Input name");
		Scanner myScanner = new Scanner(System.in);
		String name = myScanner.nextLine();

		try {

			Comics comicName = new Comics();
			comicName.setName(name);

			if (comicList.contains(comicName)) {

				System.out.println("Yes");
				comicList.remove(comicName);
			} else {
				System.out.println("No");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
