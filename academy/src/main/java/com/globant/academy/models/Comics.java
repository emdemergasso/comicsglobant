package com.globant.academy.models;

public class Comics {

	private String name;
	private String genre;
	private String editorial;
	private int quantity;
	
	public Comics(){}

	public Comics(String name, String genre, String editorial,
			int quantity) {

		this.name = name;
		this.genre = genre;
		this.editorial = editorial;
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Nombre | Genre | Editorial | Quantity\n"+name +" | "+ genre +" | "+ editorial +" | "+ quantity;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if (this == obj) {
			return true;
		} else if (obj == null) {

			return false;
		}else if (getClass() != obj.getClass()) {
			return false;
		}
		Comics otherComic = (Comics) obj;
		if (name == null) {
			if (otherComic.name != null) {
				return false;
			}
		} else if (!name.equals(otherComic.name)) {
			return false;
		}
		return true;
	}
}
