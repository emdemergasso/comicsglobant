package com.globant.academy.models;

public class Singleton {

	private static Singleton instance = null;
	public ListUsers listUser = new ListUsers();
	public ListComics listComic = new ListComics();
	public ListLoans listLoans = new ListLoans();
	
	private Singleton(){}

	public static Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}
		return instance;
	}
}