package com.globant.academy.models;

public class Loans {

	private String dni;
	private String comicName;
	private int cost;
	private boolean returned;

	public Loans() {

	}

	public Loans(String dni, String comicName, int cost, boolean returned) {

		this.dni = dni;
		this.comicName = comicName;
		this.cost = cost;
		this.returned = returned;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getComicName() {
		return comicName;
	}

	public void setComicName(String comicName) {
		this.comicName = comicName;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public boolean isReturned() {
		return returned;
	}

	public void setReturned(boolean returned) {
		this.returned = returned;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if (this == obj) {
			return true;
		} else if (obj == null) {

			return false;
		}else if (getClass() != obj.getClass()) {
			return false;
		}
		Loans otherLoans = (Loans) obj;
		if (dni == null) {
			if (otherLoans.dni != null) {
				return false;
			}
		} else if (!dni.equals(otherLoans.dni)) {
			return false;
		}
		return true;
	}
}
