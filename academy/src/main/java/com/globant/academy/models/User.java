package com.globant.academy.models;

public class User {

	private String username;
	private String password;
	private String firstname;
	private String lastname;
	private String dni;
	
	public User() {}

	public User(String username, String password, String firstname,
			String lastname, String dni) {
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.dni = dni;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if (this == obj) {
			return true;
		} else if (obj == null) {

			return false;
		}else if (getClass() != obj.getClass()) {
			return false;
		}
		User otherUser = (User) obj;
		if (dni == null || username == null) {
			if (otherUser.dni != null || otherUser.username != null) {
				return false;
			}
		} else if (!dni.equals(otherUser.dni) || !username.equals(otherUser.username)) {
			return false;
		}
		return true;
	}
	
	

}
