package com.globant.academy.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import com.globant.academy.models.User;
import com.globant.academy.utils.Constant;

public class ListUsers {

	private ArrayList<User> userList = new ArrayList<User>();

	public boolean addUser(String username, String password, String firstname, String lastname, String dni) {

		try {

			userList.add(new User(username, password, firstname, lastname, dni));
			return true;

		} catch (Exception e) {

			Constant.print("Something went wrong, try again.");
			e.printStackTrace();
			return false;
		}
	}

	public User getUser(String username, String password) {

		try {

			for (User user : userList) {

				if (user.getUsername().equalsIgnoreCase(username) && user.getPassword().equalsIgnoreCase(password)) {

					return user;
				}
			}

		} catch (Exception e) {

			Constant.print("User not found");
			e.printStackTrace();
		}

		return null;
	}

	public void getAllUsers() {

		Constant.print("Firstname | Lastname | D.N.I | Username | Password");

		for (User users : userList) {

			System.out.println(users.getFirstname() + " " + users.getLastname() + " " + users.getDni() + " "
					+ users.getUsername() + " " + users.getPassword());
		}
	}

	public void deleteUserByDni() {

		Constant.print("Delete user by dni");
		Constant.print("Input dni");
		Scanner myScanner = new Scanner(System.in);
		String dni = myScanner.nextLine();

		try {

			User userDni = new User();
			userDni.setDni(dni);

			if (userList.contains(userDni)) {

				System.out.println("Yes");
				userList.remove(userDni);
			} else {
				System.out.println("No");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}