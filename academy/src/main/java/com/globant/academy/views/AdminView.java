package com.globant.academy.views;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.globant.academy.models.Singleton;
import com.globant.academy.utils.Constant;

public class AdminView {

	Scanner sc = new Scanner(System.in);
	Scanner scMenu = new Scanner(System.in);
	int choosenOption;

	public AdminView() {

		do {

			Constant.print("\nAdmin Panel\n----------------------------\n");
			Constant.print("0. User register");
			Constant.print("1. User delete");
			Constant.print("2. User list");
			Constant.print("3. Comic register");
			Constant.print("4. Comic delete");
			Constant.print("5. Comic list");
			Constant.print("6. Loan register");
			Constant.print("7. Loan delete");
			Constant.print("8. Loan list");
			Constant.print("9. Logout\n");

			try {

				choosenOption = sc.nextInt();

				switch (choosenOption) {

				case 0:

					registerUser();

					break;

				case 1:

					Singleton.getInstance().listUser.deleteUserByDni();

					break;

				case 2:
					Singleton.getInstance().listUser.getAllUsers();
					break;

				case 3:

					registerComic();

					break;

				case 4:

					Singleton.getInstance().listComic.deleteComicByName();

					break;

				case 5:
					Singleton.getInstance().listComic.getAllComics();
					break;

				case 6:

					registerLoan();

					break;

				case 7:

					Singleton.getInstance().listLoans.deleteLoanByDni();

					break;

				case 8:
					Singleton.getInstance().listLoans.getAllLoans();
					break;

				case 9:
					new MainPanel();
					break;

				default:
					break;
				}

			} catch (InputMismatchException e) {

				Constant.print("\nInvalid Option");
				sc.next();
			}

		} while (choosenOption != 9 && sc.hasNextLine());
	}

	private void registerUser() {

		String username;
		String password;
		String firstname;
		String lastname;
		String dni;

		do {

			Constant.print("\nPlease enter username: ");
			username = scMenu.nextLine();
			Constant.print("\nPlease enter password: ");
			password = scMenu.nextLine() + "\n";
			Constant.print("\nPlease enter firstname: ");
			firstname = scMenu.nextLine() + "\n";
			Constant.print("\nPlease enter lastname: ");
			lastname = scMenu.nextLine() + "\n";
			Constant.print("\nPlease enter dni: ");
			dni = scMenu.nextLine();

			Singleton.getInstance().listUser.addUser(username, password, firstname, lastname, dni);
			
			
		} while (username.isEmpty() && password.isEmpty() && firstname.isEmpty() && lastname.isEmpty()
				&& dni.isEmpty());

	}

	private void registerComic() {

		String name;
		String genre;
		String editorial;
		int quantity = 1;

		do {

			Constant.print("\nPlease enter name: ");
			name = scMenu.nextLine();
			Constant.print("\nPlease enter genre: ");
			genre = scMenu.nextLine() + "\n";
			Constant.print("\nPlease enter editorial: ");
			editorial = scMenu.nextLine() + "\n";
			Constant.print("\nPlease enter quantity: ");
			quantity = scMenu.nextInt();

			Singleton.getInstance().listComic.addComic(name, genre, editorial, quantity);

		} while (name.isEmpty() && genre.isEmpty() && editorial.isEmpty());
	}

	private void registerLoan() {

		String dni;
		String comicName;
		int cost = 0;

		do {

			Constant.print("\nPlease enter dni: ");
			dni = scMenu.nextLine();
			Constant.print("\nPlease enter comic name: ");
			comicName = scMenu.nextLine() + "\n";
			Constant.print("\nPlease enter cost: ");
			cost = scMenu.nextInt();

			Singleton.getInstance().listLoans.addLoan(dni, comicName, cost, false);

		} while (dni.isEmpty() && comicName.isEmpty() && cost == 0);
	}
}
