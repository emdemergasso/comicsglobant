package com.globant.academy.views;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.globant.academy.models.Singleton;
import com.globant.academy.utils.Constant;

public class UserView {

	Scanner sc = new Scanner(System.in);
	Scanner scMenu = new Scanner(System.in);
	int choosenOption;

	public UserView() {

		do {

			Constant.print("\nUser Panel\n----------------------------\n");
			Constant.print("0. Comic list");
			Constant.print("1. Loan register");
			Constant.print("2. Logout\n");

			try {

				choosenOption = sc.nextInt();

				switch (choosenOption) {

				case 0:
					Singleton.getInstance().listComic.getAllComics();
					break;

				case 1:
					registerLoan();
					break;

				case 2:
					new MainPanel();
					break;

				default:
					break;
				}
				
			} catch (InputMismatchException e) {
				Constant.print("\nInvalid Option");
				sc.next();
			}

		} while (choosenOption != 2 && sc.hasNextLine());

	}

	private void registerLoan() {

		String dni;
		String comicName;
		int cost = 0;

		do {

			Constant.print("\nPlease enter dni: ");
			dni = scMenu.nextLine();
			Constant.print("\nPlease enter comic name: ");
			comicName = scMenu.nextLine() + "\n";

			Singleton.getInstance().listLoans.addLoan(dni, comicName, cost, false);

		} while (dni.isEmpty() && comicName.isEmpty());
	}

}
