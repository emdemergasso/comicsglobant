package com.globant.academy.views;

import java.util.Scanner;

import com.globant.academy.controllers.LoginController;
import com.globant.academy.utils.Constant;

public class LoginView {

	public LoginView() {

		Scanner sc = new Scanner(System.in);
		Constant.print("\nWelcome to login System\n----------------------");
		String username = "";
		String password = "";
		LoginController loginController = new LoginController();
		int value;

		do {

			Constant.print("\nEnter username: ");
			username = sc.nextLine();
			Constant.print("\nEnter password: ");
			password = sc.nextLine();

			value = loginController.loginSystem(username, password);

		} while (value == 0);
		
		switch (value) {
		
		case 1:
			new UserView();
			break;
		
		case 2:
			new AdminView();
			break;

		default:
			break;
		}
		
		sc.close();
	}
}