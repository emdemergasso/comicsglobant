package com.globant.academy.views;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.globant.academy.utils.Constant;
import com.globant.academy.utils.HardcodedElements;

public class MainPanel {

	Scanner sc = new Scanner(System.in);
	int choosenOption;

	public MainPanel() {

		Constant.print("\n Welcome to main panel, choose an option.\n ---------------------------------------");

		do {

			Constant.print("\n0. Login");
			Constant.print("1. Comic List");
			Constant.print("2. Exit \n");

			try {
				choosenOption = sc.nextInt();
				switch (choosenOption) {

				case 0:
					new LoginView();
					break;

				case 1:
					new ComicsView();
					break;

				case 2:
					Constant.print("\nGoodbye, see you next time.");
					System.exit(0);
					break;

				default:
					break;
				}

			} catch (InputMismatchException e) {
				Constant.print("\nInvalid option");
				sc.next();
			}

		} while (choosenOption != 2 && sc.hasNextLine());
	}
}
