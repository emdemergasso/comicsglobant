package com.globant.academy.utils;

import com.globant.academy.models.Singleton;

public class HardcodedElements {

	public HardcodedElements() {

		//Comics
		Singleton.getInstance().listComic.addComic("Rojo", "Action", "Iguana", 1);
		Singleton.getInstance().listComic.addComic("Azul", "Horror", "Quilmes", 2);
		Singleton.getInstance().listComic.addComic("Amarillo", "Science fiction", "Budweiser", 3);
		Singleton.getInstance().listComic.addComic("Verde", "Adventure", "Isenbeck", 4);
		Singleton.getInstance().listComic.addComic("Blanco", "Suspense", "Brahma", 5);
		
		//Users
		Singleton.getInstance().listUser.addUser("test", "test", "Pepe", "Argento", "1111");
		Singleton.getInstance().listUser.addUser("marco", "marco", "Ricardo", "Arjona", "2222");
		Singleton.getInstance().listUser.addUser("ocram", "ocram", "La", "Kristi", "3333");
		Singleton.getInstance().listUser.addUser("comar", "comar", "El", "braian", "4444");
		
		//Loans
		Singleton.getInstance().listLoans.addLoan("1111", "Verde", 20, false);
		Singleton.getInstance().listLoans.addLoan("2222", "Verde", 20, false);
		Singleton.getInstance().listLoans.addLoan("2222", "Blanco", 20, false);
	}
}
