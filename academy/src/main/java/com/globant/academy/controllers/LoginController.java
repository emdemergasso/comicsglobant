package com.globant.academy.controllers;

import com.globant.academy.models.Singleton;
import com.globant.academy.models.User;
import com.globant.academy.utils.Constant;

public class LoginController {

	public int loginSystem(final String username, final String password) {

		if (username.toLowerCase().equals("sheldon")
				&& password.toLowerCase().equals("bazinga")) {

			return 2;

		} else {

			User user = Singleton.getInstance().listUser.getUser(username,
					password);
			if (user != null) {

				Constant.print("\nWellcome " + user.getUsername() + " - User Panel\n----------------------------\n");
				return 1;
			
			} else {

				Constant.print("\nUser not found.");
			}
		}

		return 0;
	}
}