package com.globant.academy;

import com.globant.academy.utils.HardcodedElements;
import com.globant.academy.views.MainPanel;

public class App {

	public static void main(String[] args) {

			new HardcodedElements();
			new MainPanel();
	}
}
